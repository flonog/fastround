﻿using System.Linq;
using Smod2;
using Smod2.API;
using Smod2.EventHandlers;
using Smod2.Events;
using UnityEngine;
using System.Collections;

namespace FastRound
{
    internal class EventHandlers : IEventHandlerRoundStart, IEventHandlerDoorAccess, IEventHandlerSpawn, IEventHandlerPocketDimensionExit, IEventHandlerRoundEnd
    {
        private Main main;

        public EventHandlers(Main main)
        {
            this.main = main;
        }
        public void OnDoorAccess(PlayerDoorAccessEvent ev)
        {
            if(Main.enable)
            {
                if(ev.Player.TeamRole.Team == Smod2.API.Team.SCIENTIST || ev.Player.TeamRole.Team == Smod2.API.Team.CLASSD)
                {
                    if (ev.Door.Name == "CHECKPOINT_LCZ_A")
                    {

                        if (ev.Player.GetCurrentItem().ItemType == ItemType.O5_LEVEL_KEYCARD || ev.Player.GetCurrentItem().ItemType == ItemType.FACILITY_MANAGER_KEYCARD)
                        {
                            if (ev.Player.TeamRole.Team == Smod2.API.Team.SCIENTIST)
                            {
                                PluginManager.Manager.Server.Round.Stats.ScientistsEscaped++;
                                ev.Player.ChangeRole(Role.NTF_SCIENTIST, true, true, true, true);
                            }
                            else
                            {
                                PluginManager.Manager.Server.Round.Stats.ClassDEscaped++;
                                ev.Player.ChangeRole(Role.CHAOS_INSURGENCY, true, true, true, true);
                            }
                            ev.Allow = false;
                        }
                        else
                        {
                            ev.Allow = false;
                        }
                    }
                    else if (ev.Door.Name == "CHECKPOINT_LCZ_B")
                    {
                        ev.Player.PersonalBroadcast(10, "<color=#ffe500><b>FastRound</b></color>\nPour sortir, veuillez ouvrir le Checkpoint A.", true);
                        ev.Allow = false;
                    }
                }

                if(ev.Door.Name == "LCZ_ARMORY")
                {
                    if(ev.Player.GetCurrentItem().ItemType == ItemType.MTF_COMMANDER_KEYCARD)
                    {
                        ev.Allow = true;
                    }
                    else
                    {
                        ev.Allow = false;
                        ev.Player.PersonalBroadcast(5, "<color=#ffe500><b>FastRound</b></color>\nSeul une carte commandant peut ouvrir cette porte.", true);
                    }
                }
            }
        }

        public void OnPocketDimensionExit(PlayerPocketDimensionExitEvent ev)
        {
            if (Main.enable)
            {
                ev.ExitPosition = PluginManager.Manager.Server.Map.GetRandomSpawnPoint(Role.SCP_173);
            }
        }

        public void OnRoundEnd(RoundEndEvent ev)
        {
            Main.enable = false;
        }

        public void OnRoundStart(RoundStartEvent ev)
        {
            if(ev.Server.GetPlayers().Count <= main.GetConfigInt("fr_nb_player") && main.GetConfigBool("fr_enabled"))
            {
                Room checkpoint = PluginManager.Manager.Server.Map.Get079InteractionRooms(Scp079InteractionType.CAMERA).Where(room => room.RoomType == RoomType.CHECKPOINT_A).First();
                Vector pos = new Vector(checkpoint.Position.x, checkpoint.Position.y + 3, checkpoint.Position.z);
                PluginManager.Manager.Server.Map.Broadcast(10, "<color=#ffe500><b>FastRound</b></color>\nCeci est un FastRound. La partie a été racourcie\n Plus d'info : ~ ou ù", true);
                Main.enable = true;

                foreach(Smod2.API.Item i in PluginManager.Manager.Server.Map.GetItems(ItemType.P90, true))
                {
                    PluginManager.Manager.Server.Map.SpawnItem(ItemType.E11_STANDARD_RIFLE, i.GetPosition(), i.GetPosition());
                    i.Remove();
                }

                foreach (Smod2.API.Item i in PluginManager.Manager.Server.Map.GetItems(ItemType.MP4, true))
                {
                    PluginManager.Manager.Server.Map.SpawnItem(ItemType.MICROHID, i.GetPosition(), i.GetPosition());
                    i.Remove();
                }

                foreach(var elev in PluginManager.Manager.Server.Map.GetElevators())
                {
                    elev.Use();
                    elev.Locked = true;
                }

                foreach (Player p in PluginManager.Manager.Server.GetPlayers(Smod2.API.Team.SCP))
                {
                    p.Teleport(PluginManager.Manager.Server.Map.GetRandomSpawnPoint(Role.SCP_173));

                }

                foreach (Player p in PluginManager.Manager.Server.GetPlayers(Smod2.API.Team.CHAOS_INSURGENCY))
                {
                    p.Teleport(pos);
                }

                foreach (Player p in PluginManager.Manager.Server.GetPlayers(Smod2.API.Team.NINETAILFOX))
                {
                    p.Teleport(pos);
                }
            }
            else
            {
                Main.enable = false;
            }
        }

        public void OnSpawn(PlayerSpawnEvent ev)
        {   
            if (Main.enable)
            {
                Room checkpoint = PluginManager.Manager.Server.Map.Get079InteractionRooms(Scp079InteractionType.CAMERA).Where(room => room.RoomType == RoomType.CHECKPOINT_A).First();
                Vector pos = new Vector(checkpoint.Position.x, checkpoint.Position.y + 3, checkpoint.Position.z);
                if (ev.Player.TeamRole.Team == Smod2.API.Team.SCP)
                {
                    ev.Player.Teleport(PluginManager.Manager.Server.Map.GetRandomSpawnPoint(Role.SCP_173));
                }
                if (ev.Player.TeamRole.Team == Smod2.API.Team.NINETAILFOX)
                {
                    ev.SpawnPos = pos;
                    ev.Player.SetHealth(ev.Player.GetHealth() / 2);
                }
                else if(ev.Player.TeamRole.Team == Smod2.API.Team.CHAOS_INSURGENCY)
                {
                    ev.SpawnPos = pos;
                }
            }
        }
    }
}