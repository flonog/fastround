﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smod2;
using Smod2.Attributes;

namespace FastRound
{
    [PluginDetails(author = "Flo - Fan", configPrefix = "fr", description = "Make a round faster when there isn't enough Player", id = "flo.fr", name = "FastRound", SmodMajor = 3, SmodMinor = 4, SmodRevision = 1, version = "0.0.1")]

    public class Main : Plugin
    {

        public static bool enable;

        public override void Register()
        {
            this.AddConfig(new Smod2.Config.ConfigSetting("fr_enabled", true, true, "Enabled the plugin."));
            this.AddConfig(new Smod2.Config.ConfigSetting("fr_nb_player", 5, true, "The number max of player to start a Fast Round"));

            this.AddEventHandlers(new EventHandlers(this));
        }

        public override void OnEnable()
        {
            this.Info("FastRound enabled successfuly.");
        }

        public override void OnDisable()
        {
            this.Info("FastRound disabled.");
        }

        
    }
}
